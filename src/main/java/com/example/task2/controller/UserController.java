package com.example.task2.controller;

import com.example.task2.servise.UserService;
import com.example.task2.enums.Code;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.UUID;

import static com.example.task2.responsedata.ResponseData.ok;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/user")
public class UserController {

    private final UserService userService;


    @GetMapping("/{id}")
    public ResponseEntity<?> getUSerInfo(@PathVariable UUID id) {
        return ok(Code.SUCCESS, userService.getUSerInfo(id));
    }
}
