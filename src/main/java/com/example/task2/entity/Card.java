package com.example.task2.entity;

import com.example.task2.enums.CardType;
import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;
import org.hibernate.annotations.GenericGenerator;

import java.util.List;
import java.util.UUID;

@Getter
@Setter
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "cards")
public class Card extends AuditingEntity{

    @Transient
    public static final String sequenceName = "card_id_seq";

    @Id
    @GeneratedValue(generator = sequenceName, strategy = GenerationType.SEQUENCE)
    @GenericGenerator(
            name = sequenceName,
            strategy = "org.hibernate.id.UUIDGenerator")
    private UUID id;

    @JsonBackReference(value = "user_id")
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id")
    private User user;

    @Column(name = "card_name")
    private String cardName;

    @Enumerated(EnumType.STRING)
    @Column(name = "card_type", length = 3)
    private CardType cardType;

    @Column(name = "card_number", length = 20)
    private String cardNumber;

    @Column(name = "card_expire", length = 7)
    private String cardExpire;

    private long balance;

    private int status;

    @JsonManagedReference(value = "card")
    @OneToMany(mappedBy = "card")
    @JsonIgnore
    private List<Transaction> transactions;
}
