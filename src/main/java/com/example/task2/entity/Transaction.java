package com.example.task2.entity;

import com.example.task2.enums.Type;
import com.fasterxml.jackson.annotation.JsonBackReference;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;
import org.hibernate.annotations.GenericGenerator;

import java.util.UUID;

@Getter
@Setter
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "transactions")
public class Transaction extends AuditingEntity{

    @Transient
    public static final String sequenceName = "transaction_id_seq";

    @Id
    @GeneratedValue(generator = sequenceName, strategy = GenerationType.SEQUENCE)
    @GenericGenerator(
            name = sequenceName,
            strategy = "org.hibernate.id.UUIDGenerator")
    private UUID id;

    @JsonBackReference(value = "user_id")
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id")
    private User user;

    @JsonBackReference(value = "card_id")
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "card_id")
    private Card card;

    @Enumerated(EnumType.STRING)
    private Type type;

    private long amount;

    @Column(name = "old_balance")
    private long oldBalance;

    @Column(name = "new_balance")
    private long newBalance;
}
