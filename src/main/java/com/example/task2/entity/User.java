package com.example.task2.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.GenericGenerator;
import org.springframework.data.annotation.CreatedDate;

import java.sql.Timestamp;
import java.time.Instant;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@Getter
@Setter
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "users")
public class User extends AuditingEntity{

    @Transient
    public static final String sequenceName = "user_id_seq";

    @Id
    @GeneratedValue(generator = sequenceName, strategy = GenerationType.SEQUENCE)
    @GenericGenerator(
            name = sequenceName,
            strategy = "org.hibernate.id.UUIDGenerator")
    private UUID id;

    private String fullName;

    private String eMail;

    private long phoneNumber;

    private int status;

    private Date dob;

    @JsonManagedReference(value = "user")
    @OneToMany(mappedBy = "user")
    @JsonIgnore
    private List<Card> cards;
}
