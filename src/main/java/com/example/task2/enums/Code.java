package com.example.task2.enums;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
public enum Code {

    SUCCESS,
    DATA_NOT_ACTIVE,
    ACCESS_DENIED,
    DATA_NOT_FOUND,
    INVALID_DATA,
    UNEXPECTED_ERROR
}
