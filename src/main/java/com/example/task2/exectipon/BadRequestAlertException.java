package com.example.task2.exectipon;

import com.example.task2.enums.Code;
import lombok.Getter;

import static com.example.task2.message.MessageKey.CARD_NOT_FOUND;
import static com.example.task2.message.MessageKey.USER_NOT_FOUND;
import static com.example.task2.enums.Code.DATA_NOT_FOUND;


@Getter
public class BadRequestAlertException extends RuntimeException {

    private final Code code;

    public BadRequestAlertException(String message, Code code) {
        super(message);
        this.code = code;
    }

    public static BadRequestAlertException badRequestAlertException(String key, Code code) {
        return new BadRequestAlertException(key, code);
    }

    public static BadRequestAlertException userNotFound() {
        return badRequestAlertException(USER_NOT_FOUND, DATA_NOT_FOUND);
    }

    public static BadRequestAlertException addressNotFound() {
        return badRequestAlertException(CARD_NOT_FOUND, DATA_NOT_FOUND);
    }
}
