package com.example.task2.mapper;

import com.example.task2.entity.Card;
import com.example.task2.entity.Transaction;
import com.example.task2.entity.User;
import com.example.task2.payLoad.UserResponse;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring", imports = {UserResponse.class, User.class, Card.class, Transaction.class})
public interface UserMapper {


    UserResponse toResponse(User user);
}
