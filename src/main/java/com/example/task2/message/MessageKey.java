package com.example.task2.message;

import lombok.Getter;

@Getter
public final class MessageKey {

    public static final String USER_NOT_FOUND = "user.not.found";

    public static final String CARD_NOT_FOUND = "card.not.found";

    public static final String PARAMETERS_NOT_VALID = "params.not.valid";

    public static final String SUCCESS_MESSAGE = "success.message";
    public MessageKey() {

    }
}
