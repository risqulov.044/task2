package com.example.task2.payLoad;

import com.example.task2.entity.Card;
import lombok.*;

import java.time.Instant;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class UserResponse {

    private UUID id;

    private String fullName;

    private String eMail;

    private long phoneNumber;

    private int status;

    private Date dob;

    private Instant createdDate;

    private List<Card> cards;
}
