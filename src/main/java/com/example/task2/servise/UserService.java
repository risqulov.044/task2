package com.example.task2.servise;

import com.example.task2.entity.User;
import com.example.task2.payLoad.UserResponse;

import java.util.UUID;

public interface UserService {

    UserResponse getUSerInfo(UUID id);
}
