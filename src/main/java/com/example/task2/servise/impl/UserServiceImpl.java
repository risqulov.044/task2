package com.example.task2.servise.impl;

import com.example.task2.exectipon.BadRequestAlertException;
import com.example.task2.mapper.UserMapper;
import com.example.task2.payLoad.UserResponse;
import com.example.task2.repository.UserRepository;
import com.example.task2.servise.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Service
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {


    private final UserRepository userRepository;

    private final UserMapper userMapper;


    @Override
    public UserResponse getUSerInfo(UUID id) {
        return userRepository.findById(id)
                .map(userMapper::toResponse)
                .orElseThrow(BadRequestAlertException::userNotFound);
    }
}
